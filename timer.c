#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>

typedef struct {
		
		int time;
		int port;
		long seq;
		char type;
	
} Timer_message;


/* Forward declare a type "node" to be a struct. */
typedef struct node node;
/* Declare the struct with integer members x, y */
struct node {
	
   int 	  time;
   int    port;
   long   seq;
   node * next;
   node * prev;
   
};

typedef struct linked_list linked_list;
struct linked_list {
	node * head;
	node * tail;
	int  	len;
};

/* Insert c between a and b */
void add_in_between(node * a, node *c, node *b )
{
	if ( a == NULL)
		{
			b->next = c;
			b->prev = NULL;
			c->prev = b;
			c->time -= b->time; 
			return;
		}
	if ( c == NULL)
		{
			b->prev = a;
			b->next = NULL;
			a->next = b;
			b->time -= a->time;
		}
	
	b->prev = a;
	b->next = c;
	
	a->next = b;
	c->prev = b;
	
	b->time -= a->time;
	c->time -= b->time;
	
}

void add_node_to_list(linked_list * A, node * b)
{
	int val = b->time;
	
	if ( A->len == 0)
		{
		A->head = b;
		A->tail = b;
		A->len += 1;
		return ;	
		}
		
	node * h = A->head;
	node * t = A->tail;
	node * p = NULL;
	
	if (h->next == NULL)
		printf(" val is %d\n", val);
	
	while ( (h->next != NULL) && (val > h->time))
	{
		//printf( " The values encountered are %d , %d\n", val,h->time);
		p = h;
		val -= h->time;  
		h = h->next;
		//printf(" VV %d \n",val);
	}
	
	if ( val > h->time)
	{
	
		/* Update timer */
		//printf(" The values in grater loop are  %d %d\n", val, h->time);
		b->time = val - h->time;
		//printf(" The value of the new node is %d \n", b->time);	
		/* Update connections of b */
		b->prev = h;
		h->next = b;
		A->tail = b;	
	}
	else
	
	{
		printf(" The values in smaller loop are  %d %d\n", val, h->time);
		/* Update timer */
		b->time = val;
		h->time -= val;
		
		/* Update connections of b */
		b->next = h;
		b->prev = h->prev;
			
		/* Update connections of linked lits */
		if ( p != NULL)
			{ 
				p->next = b;
			}
		else
			{
				A->head = b;
			}

		h->prev = b;
	}
	
	A->len += 1	;
	printf(" Added a new node \n");
	
}

void print_linked_list(linked_list * A)
{
	node * h = A->head;
	int count = 0;
	printf(" List is -> ");
	while ( h != NULL)
		{
			count += 1;
			//printf(" value at %d is : %d and seq_no is %lu\n",count,h->time,h->seq);
			printf(" ( %d  : %lu) <-> ",h->time ,h->seq);
			h = h->next;
		}
	printf(" NIL.   Length is %d \n",A->len);
}

void delete_node(linked_list * A)
{
	/* Always delete the head only */
	if (A== NULL)
		return;
	if (A->len == 0)
		return;
	if (A->len == 1)
	{
		A->head = NULL;
		A->tail = NULL;
		A->len - 0;
	}
	node * a = A->head;
	A->head = a->next;
	A->head->prev = NULL;
	A->len -= 1;
}

void update_head( linked_list * A)
{
	if ( A == NULL)
	{
		return;
	}
	if (A->head == NULL)
		return;
	else ( A->head != NULL)
	{
		//printf("Decremnting time \n");
		A->head->time -= 1;
		while( (A->head != NULL) && (A->head->time == 0))
		 {
			 printf(" Timer: Deleting node with %lu seq no from linked list \n",A->head->seq);
			 delete_node(A);
		 }
	 }
}

void delete_node_of_seq_no(linked_list * A, long seq_no)
{

	node *p;
	node * h = A->head;
	
	if ( (A->len == 0) | (h == NULL ))
		{
		printf(" The linked list is empty, cant delete , sorry \n");
		return ;
		}

	if (h->seq == seq_no)
		{
		delete_node(A);
		printf(" The seq no %lu is deleted \n", seq_no);
		return;
		}
				
	while( h->next != NULL)
	{
		if (h->seq == seq_no)
		{ 
		p->next = h->next;
		h->next->prev = p;
		printf(" The seq no %lu is deleted \n", seq_no);
		A->len -= 1;
		return;
		}
		p = h;
		h = h->next;
	}
	
	if (h->seq == seq_no)
		{	
		p->next = NULL;
		printf(" The seq no %lu is deleted \n", seq_no);
		A->len -= 1;
		return;
		}						
	//printf(" The linked list could not find element\n");
}


int main(int argc, char * argv[])
{


/* Create a linked list */
linked_list A = {.head = NULL, .tail = NULL , .len =0 };
linked_list * B = &A;

Timer_message * t_msg = malloc(sizeof(Timer_message));

if (argc >= 1)
{
printf(" Timer : The Port number passed  is %s\n",argv[1]);
}

int PORT_NUM;
PORT_NUM = atoi(argv[1]);

// Variable declarations
struct sockaddr_in server_address, client_address;


// Socket creation
printf("Timer: Creating socket at server end\n");

int timer_socket = socket(AF_INET,SOCK_DGRAM,0);
if (timer_socket < 0)
        {
        printf("Timer: Socket error: %d\n",errno);
        exit(0);
        }

printf("Timer: Socket succesfull\n");

server_address.sin_family = AF_INET;
server_address.sin_port = htons(PORT_NUM);
server_address.sin_addr.s_addr = htons(INADDR_ANY);

int server_addr_len = sizeof(server_address);

// Bind the socket

int bind_id = bind(timer_socket, (struct sockaddr *)&server_address, sizeof(server_address));

if (bind_id < 0)
{
printf("Timer: Socket Bind Error: %d\n", errno);
exit(0);
}
printf("Timer: Binding timer socket successful\n");

// Listen ( Start listening to connections)
printf("Timer: starting to listen timer messages from Driver\n");
int id;
int time1=0;
while( 1==1)
	{
		
		struct timeval tv;
		tv.tv_sec=2;
		tv.tv_usec = 000000;
		
		int nflag,rv;
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(timer_socket,&readfds);
		nflag = timer_socket + 1;
		rv = select(nflag,&readfds,NULL,NULL,&tv);
		if (rv == -1)
        {
                perror("select"); // error occurred in select()
        }
        if (rv == 0)
        {
            printf("-\n");
        }
        if ( (rv != -1) && (rv != 0))
        {
            if (FD_ISSET(timer_socket, &readfds))
            {
				id = recvfrom(timer_socket,(char*)t_msg,sizeof(Timer_message),0,(struct sockaddr *)&server_address,&server_addr_len);
				if ( id < 0)
				{
				printf("Timer: Message receive failed \n");
				}
				printf("Timer: Message received from Driver :\n");
				
				node * b = malloc(sizeof(node));
					b->time = t_msg->time;
					b->port = t_msg->port;
					b->seq = t_msg->seq;
					
				if (t_msg->type == 's')
				{
					printf("Adding - seq to List : %lu , time : %d\n", t_msg->seq, t_msg->time);
					add_node_to_list(B, b);
					//print_linked_list(B);
				}
				if (t_msg->type == 'c')
				{
					printf("Removing -node of seq_no = %lu from List\n", t_msg->seq);
					delete_node_of_seq_no(B, t_msg->seq);
					//print_linked_list(B);
				}			
			}
			
			sleep(2);
		}
		update_head(B);
		printf("time : %d\n",++time1);
		print_linked_list(B);
	}
	
	close(timer_socket);
	printf("Timer:Sockets Closed \n");
	printf("Timer: Program Ended\n");
}

