#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>

typedef struct {
		
		int time;
		int port;
		long seq;
		char type;
	
} Timer_message;


/* Forward declare a type "node" to be a struct. */
typedef struct node node;
/* Declare the struct with integer members x, y */
struct node {
	
   int 	  time;
   int    port;
   long   seq;
   node * next;
   node * prev;
   
};

typedef struct linked_list linked_list;
struct linked_list {
	node * head;
	node * tail;
	int  	len;
};

/* Insert c between a and b */
void add_in_between(node * a, node *c, node *b )
{
	if ( a == NULL)
		{
			b->next = c;
			b->prev = NULL;
			c->prev = b;
			c->time -= b->time; 
			return;
		}
	if ( c == NULL)
		{
			b->prev = a;
			b->next = NULL;
			a->next = b;
			b->time -= a->time;
		}
	
	b->prev = a;
	b->next = c;
	
	a->next = b;
	c->prev = b;
	
	b->time -= a->time;
	c->time -= b->time;
	
}

void add_node_to_list(linked_list * A, node * b)
{
	int val = b->time;
	
	if ( A->len == 0)
		{
		A->head = b;
		A->tail = b;
		A->len += 1;
		return ;	
		}
		
	node * h = A->head;
	node * t = A->tail;
	node * p = NULL;
	
	if (h->next == NULL)
		printf(" val is %d\n", val);
	
	while ( (h->next != NULL) && (val > h->time))
	{
		printf( " The values encountered are %d , %d\n", val,h->time);
		p = h;
		val -= h->time;  
		h = h->next;
		printf(" VV %d \n",val);
	}
	
	if ( val > h->time)
	{
	
		/* Update timer */
		printf(" The values in grater loop are  %d %d\n", val, h->time);
		b->time = val - h->time;
		printf(" The value of the new node is %d \n", b->time);	
		/* Update connections of b */
		b->prev = h;
		h->next = b;
		A->tail = b;	
	}
	else
	
	{
		printf(" The values in smaller loop are  %d %d\n", val, h->time);
		/* Update timer */
		b->time = val;
		h->time -= val;
		
		/* Update connections of b */
		b->next = h;
		b->prev = h->prev;
			
		/* Update connections of linked lits */
		if ( p != NULL)
			{ 
				p->next = b;
			}
		else
			{
				A->head = b;
			}

		h->prev = b;
	}
	
	A->len += 1	;
	printf(" Added a new node \n");
}

void print_linked_list(linked_list * A)
{
	node * h = A->head;
	int count = 0;
	printf(" The length is %d\n", A->len);
	while ( h != NULL)
		{
			count += 1;
			printf(" value at %d is : %d and seq_no is %lu\n",count,h->time,h->seq);
			h = h->next;
		}
}

void delete_node(linked_list * A)
{
	/* Always delete the head only */
	
	node * a = A->head;
	A->head = a->next;
	A->head->prev = NULL;
	A->len -= 1;
}

void delete_node_of_seq_no(linked_list * A, long seq_no)
{

	node *p;
	node * h = A->head;
	
	if ( (A->len == 0) | (h == NULL ))
		{
		printf(" The linked list is empty, cant delete , sorry \n");
		return ;
		}

	if (h->seq == seq_no)
		{
		delete_node(A);
		printf(" The seq no %lu is deleted \n", seq_no);
		return;
		}
				
	while( h->next != NULL)
	{
		if (h->seq == seq_no)
		{ 
		p->next = h->next;
		h->next->prev = p;
		//printf(" The seq no %lu is deleted \n", seq_no);
		A->len -= 1;
		return;
		}
		p = h;
		h = h->next;
	}
	
	if (h->seq == seq_no)
		{	
		p->next = NULL;
		printf(" The seq no %lu is deleted \n", seq_no);
		A->len -= 1;
		return;
		}						
	printf(" The linked list could not find element\n");
}


int main()
{
	/*  Create a simple node  */
node p = {.time = 10,.port =5000, .seq = 6000, .next = NULL, .prev = NULL};
node * q = &p;
printf("The p value  %d \n", q->time);

/* Create a linked list */

linked_list A = {.head = NULL, .tail = NULL , .len =0 };
linked_list * B = &A;
printf(" The length of linked_list is %d \n", B->len);


/* Add the nodes */
add_node_to_list(B,q);

print_linked_list(B);

node * n;
int time1 = 100;

int i = 1;
int b = -1;
int a[10] ;
a[0] = 10;
long seq1 = 1000;
while ((i < 10) && (time1 > 0))
{
	//time1 += b*i*i;
	a[i] = time1;   
	b *= -1;
	node * n = malloc(sizeof(node));
	
	n->time = time1;
	n->port = 5000; 
	n->seq = seq1 + i*10;
	n->next = NULL;
	n->prev = NULL;
	printf(" Value from main is %d \n",n->time);
	add_node_to_list(B,n);
	i += 1;
}
printf(" array is ");
for (i=0; i< 10; i++)
	printf("  %d , ,", a[i]);
print_linked_list(B);
delete_node(B);
print_linked_list(B);
delete_node(B);
print_linked_list(B);

node p1 = {.time = 100,.port =5000, .seq = 6000, .next = NULL, .prev = NULL};
node * q1 = &p1;
add_node_to_list(B,q1);
print_linked_list(B);
delete_node_of_seq_no(B,1060);
delete_node_of_seq_no(B,6000);
delete_node_of_seq_no(B,1070);
print_linked_list(B);
}

