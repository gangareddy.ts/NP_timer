#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>

// This program is to create a file transfer protocol server
// This program is part of Homework 2

// Assumptions
// The port through which we are transferring files is PORT_NUM
// The following attributes define the given problem specifications.

#define MSS 1000
#define SIZE_OF_BYTES 4
#define NAME_OF_FILENAME 20
#define START_SIZE_OF_BYTES 0
#define START_NAME_OF_FILENAME 4
#define MAX_SIZE_OF_FNAME 256
#define BAD_FNAME 777
#define HEADER_SIZE 16

typedef struct 
{
struct sockaddr_in header;
char body[MSS-HEADER_SIZE];
} troll_message;

void print_message_from_client(char * sendbuffer)
{
printf("TCPD Server: %s\n",sendbuffer);
}


void print_message_of_server(char * sendbuffer)
{
printf("TCPD Server: %s\n",sendbuffer);
}


int main(int argc, char *argv[] )
{
char * troll_host;
int PORT_NUM;
int PORT_NUM_OUT = 6666;		// PORT at which it communicates to FTP server

if (argc > 1)
{
	printf(" The TCPD Server started running \n");
	printf(" The Port ( Receiving) number passed  is %s\n",argv[1]);
	printf(" The Port ( sending) number passed  is %d\n",PORT_NUM_OUT);
	PORT_NUM = atoi(argv[1]);	// PORT at which server is supposed to listen

}

else
{
	printf(" The arguments for portnumber and localhost are not passed  \n");
	printf(" The TCPD server is running locally and port is %d\n",PORT_NUM);
	exit(1);
}


// Variable declarations
struct sockaddr_in server_address, troll_address;
int new_client,client_length;
int pid;

// Socket creation to listen

printf("TCPD: Creating socket at server to receive \n");
int troll_socket;
troll_socket = socket(AF_INET,SOCK_DGRAM,0);
if (troll_socket < 0)
        {
        printf("TCPD: Socket error: %d\n",errno);
        exit(0);
        }

printf("TCPD: Socket succesfull\n");

troll_address.sin_family = AF_INET;
troll_address.sin_port = htons(PORT_NUM);
troll_address.sin_addr.s_addr = htons(INADDR_ANY);


// Bind the socket
int bind_id = bind(troll_socket, (struct sockaddr *)&troll_address, sizeof(troll_address));
if (bind_id < 0)
{
printf("TCPD: Socket Bind Error: %d\n", errno);
exit(0);
}
printf("TCPD: Binding Server socket successful\n");


// Socket creation to send data to FTPS.c
printf("TCPD: Creating socket at server to send \n");
int server_socket;
server_socket = socket(AF_INET,SOCK_DGRAM,0);
if (server_socket < 0)
        {
        printf("TCPD troll: Socket error: %d\n",errno);
        exit(0);
        }
printf("TCPD troll: Socket succesfull\n");

struct hostent * server_addr;
server_addr = gethostbyname("localhost");
if (server_addr == NULL)
{
        printf("TCPD Server: Could not get hostanme: %d\n",errno);
}

// Multiple ways to assign server adress 
memcpy(&server_address.sin_addr, server_addr->h_addr_list[0],server_addr->h_length);
server_address.sin_family = AF_INET;
server_address.sin_port = htons(PORT_NUM_OUT);

char recvbuffer[MSS];
int buf_len = MSS;
int s_name_len;
char msg[buf_len];
s_name_len = sizeof(troll_address);

int buf_count = 0;
int sent_count = 0;

while ( 1==1)
{

// Receive from client 
int mm = recvfrom(troll_socket, recvbuffer, buf_len, 0, (struct sockaddr *)&troll_address,&s_name_len);
if ( mm < 0)
{
printf("TCPD: Message receive failed @ buf count: %d\n");
}
buf_count += 1;
printf("TCPD: The message received is buf_count: %d\n",buf_count);

// Send to Troll
int ack = sendto(server_socket, recvbuffer, buf_len, 0, (struct sockaddr *)&server_address, sizeof(server_address));
if ( ack < 0)
{
printf("TCPD troll: Failed --> The message sent failed @ buf count: %d\n",sent_count);
}
sent_count += 1;
printf("TCPD: The message sent is buf_count: %d\n",sent_count);

}

}

