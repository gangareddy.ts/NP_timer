#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>

/*  Structure of message from driver to sender */

typedef struct {
		
		int time;
		int port;
		long seq_no;
		char type;
	
} Timer_message;

void starttimer( int sockfd , struct sockaddr_in timer_addr, int time, int port, long seq_no)
{
	
	Timer_message * msg;
	msg->time = time;
	msg->port = port;
	msg->seq_no = seq_no;
	msg->type = 's'; 
	
	int mm = sendto(sockfd, (char *)msg,sizeof(Timer_message),0, (struct sockaddr *)&timer_addr, sizeof(timer_addr)); 
	
	if (mm  < 0)
	{
		printf("Driver: Message sending failed with Error --  %d \n", errno);
	}
	else
	{
		printf("Driver: Message sent (seq_no = %lu  \n",seq_no);
	}
}


void canceltimer(int sockfd , struct sockaddr_in timer_addr, long seq_no)
{
	Timer_message * msg;
	msg->time = 0;
	msg->port = 0;
	msg->seq_no = seq_no;
	msg->type = 'c';
	
	int mm = sendto(sockfd, (char *)msg,sizeof(Timer_message),0, (struct sockaddr *)&timer_addr, sizeof(timer_addr)); 
	
	if (mm  < 0)
	{
		printf("Driver: Cancel Message sending failed with Error --  %d \n", errno);
	}
	else
	{
		printf("Driver: Cancel Message sent (seq_no = %lu  \n",seq_no);
	}
	
}

void print_timer_message(Timer_message * a)
{
	printf("Driver: Timer Message \n");
	
	if ( a->type == 'c')
		printf("  type: Cancel timer Msg \n");
		printf("  time: %d \n",	a->time);
		printf("  Port: %d \n",	a->port);
		printf(" seqno: %lu \n",	a->seq_no);
	if (a->type == 's')
		printf("  type: Start timer Msg \n");
		printf(" seqno: %lu \n",	a->seq_no);
}


int main(int argc, char * argv[])
{
	printf("Driver: Program Started\n");
	
	int time;
	int port;
	long seq_no;

	
	struct sockaddr_in timer_addr;
	int timer_addrlen;
	
	struct sockaddr_in out_timer_addr;
	
	//Timer_message *t_msg = malloc(sizeof(Timer_message)); 
	
	//char * timer_ip = argv[1];
	int driver_port = atoi(argv[2]);
	int timer_port = atoi(argv[3]);
	
	/* Create socket for sending timer messages to Timer */
	
	int driverSock_id;
	struct sockaddr_in driver_addr;
	int driver_addr_len;
	
	driverSock_id = socket(AF_INET,SOCK_DGRAM,0);
	
	if ( driverSock_id < 0)
		{
			printf("Driver :  Driver Socket error: %d\n",errno);
			exit(0);
		}

	printf("Driver: Driver socket successful  \n");
	
	/*
	 * No Nooed to bind socket as we are not listening back from timer 
	 * 
	driver_addr_len = sizeof(driver_addr);
	driver_addr.sin_family = AF_INET;
	driver_addr.sin_port = htons(driver_port);
	driver_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(&(driver_addr.sin_zero),0,sizeof(driver_addr.sin_zero));
	
	int bind_id = bind(driverSock_id,(struct sockaddr*)&driver_addr,driver_addr_len);
	
	if (bind_id < 0)
		{
			printf("Driver: Socket Bind Error: %d\n", errno);
			exit(0);
		}
	printf("Driver: Binding Server socket successful\n");
	
	* 
	* /
	
	/* Create sock addr for the destination (timer ) */
	
	struct hostent * server_addr;
	server_addr = gethostbyname("localhost");
	
	if (server_addr == NULL)
	{
			printf("Client: Could not get server hostanme: %d\n",errno);
	}
	
	memcpy(&timer_addr.sin_addr, server_addr->h_addr_list[0],server_addr->h_length);
	timer_addr.sin_family = AF_INET;
	timer_addr.sin_port = htons(timer_port);
	printf("passed\n");
	sleep(2);
	printf("passed\n");
	starttimer(driverSock_id, timer_addr, 20, driver_port,1);
	starttimer(driverSock_id,timer_addr,1, driver_port,2);
	starttimer(driverSock_id,timer_addr,25, driver_port,3);
	printf("passed\n");
	sleep(5);
	printf("passed\n");
	canceltimer(driverSock_id,timer_addr,2);
	starttimer(driverSock_id,timer_addr, 20, driver_port, 4);
	starttimer(driverSock_id,timer_addr,18, driver_port ,5);
	canceltimer(driverSock_id,timer_addr,4);
	canceltimer(driverSock_id,timer_addr,8); 
	printf("Passed\n");
	int close_id = close(driverSock_id);
	sleep(1);
	if (close_id > 0)
		printf("Driver: Driver Socket Closed \n");
	else
		printf("Driver: Driver Socket failed to Close \n");
	printf("Driver: Program Ended\n");
}
